---
menu:
  main:
    name: Blog
    identifier: blog
    weight: 2.5
  footer:
    name: Blog
    weight: 2
---
The Acme Blog
============

Curated content about our teams and products.
